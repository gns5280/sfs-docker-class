> From: ettiejr@ettiepy.com<br>
> Subject: Cleaning Up
>
> Hello!
>
>   We just discovered a few servers with a lot of leftover clutter.  The disks are full of old Docker containers.  Can you clean them up?
>
> Thanks!
>
> EJ

---

1. Use `docker ps --all` (or `docker ps -a`) to see all the containers on your system.
2. Run `docker rm <something>` to remove an 'Exited' container.  `<something>` should be either a Container ID or a container name.
3. Try `docker container prune` to remove all stopped containers.
4. Run `docker rmi <something>` to remove a single image.  `<something>` should be either an Image ID or an image name.
5. Use `docker images prune` to remove all images (that do not have running containers).

Bonus:
- What happens when you try to remove an image for a running container?
- What happens when you try to remove a running container?
- What does the `--force` option do in these cases?
- Investigate Docker's [system prune](https://docs.docker.com/engine/reference/commandline/system_prune/) command.
