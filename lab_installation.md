You've just started work at a new company, Aunt Ettie's House of Py.  You've got a good handle on the rest of your job, but they're a Docker shop and you've never used Docker before.  They knew that when they hired you, and they've allocated some ramp-up time for you to get familiar with Docker before jumping in feet first.

---

> From: ettiejr@ettiepy.com<br>
> Subject: Getting Started With Docker
>
> Hey, you,
>
>   Welcome!  I hope your first days is going well so far.  As discussed previously, you'll need to start learning Docker!  For today, head on over to docker.com and get Docker installed on your machine.
>
> Let me know if you have any trouble.
>
> -EJ

---

1. Head over to [docker.com](https://www.docker.com/) and find the installation page for Docker Community Edition.  (Maybe [here](https://store.docker.com/search?offering=community&type=edition).)
2. Follow the instructions for your OS.
3. Make sure that `docker info` works on your command line.
4. You may need to run `sudo usermod -a -G docker <username>` to enable docker without sudo...
