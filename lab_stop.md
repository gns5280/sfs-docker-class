> From: ettiejr@ettiepy.com<br>
> Priority: High<br> 
> Subject: Shut It Down!
>
>   I need you to shut down that mystery_machine container ASAP!  Please don't disrupt anything else in production, but get that container offline!

---

1. Use `docker stop` or `docker kill` to shut down the container from the last lab.

Bonus:
- Feel free to restart the container (see the last lab) and try both commands.
