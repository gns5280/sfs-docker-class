> From: ettiejr@ettiepy.com<br>
> Subject: Stateful Data
>
>   Passing configurations into a container is great, but we have some cases where we need a little more.  A mounted volume can hold data while a container is offline, which is really handy for storing logs after a container exits - or even keeping database files.
>
> -EJ

---

1. Create a local directory called `myDataDir` on your machine.
2. Use the `--volume $(pwd)/myDataDir:/data` option with `docker run -it ubuntu /bin/bash` to check it out.
3. In the container, write a file in the /data directory.
4. Ctrl+D to log out of the container.
5. Look in `myDataDir`.  What's there?
6. Look at the output of `docker ps`.  Is your container running?
7. Start a new container using the same docker command (with the `--volume` option)
8. What's in the `/data` directory of your new container?

Bonus:
- Use the `--volume` option to mount a volume in a Mongo database container
- `docker run -d --volume $(pwd)/$YOURLOCALDIR:/data mongo:latest`
- Check the contents of your local directory
